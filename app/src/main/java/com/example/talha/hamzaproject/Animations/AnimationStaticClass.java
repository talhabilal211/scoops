package com.example.talha.hamzaproject.Animations;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;

import com.example.talha.hamzaproject.R;


/**
 * Created by Talha Bilal on 07-Sep-17.
 */

public class AnimationStaticClass {

    public static void setScaleAnimation(View view,int duration) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(duration);
        view.startAnimation(anim);
    }
    public static void setRightToLeftAnimation(Context context,View view){
        Animation animRightToLeft = AnimationUtils.loadAnimation(context, R.anim.right_to_left_animation);
        view.startAnimation(animRightToLeft);
    }
    public static void setFadeAnimation(View view,int duration) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(duration);
        view.startAnimation(anim);
    }
    public static void startActivityWithAnimation(Activity context){
        // there is your choice you can set your style by changing in xml
        context.overridePendingTransition(R.anim.right_to_left_animation,
                R.anim.left_to_right_animation);
    }
    public static void bouncerAnimation(Context context,View view){
        final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
        BounceInterpolator interpolator = new BounceInterpolator(0.4, 10);
        myAnim.setInterpolator(interpolator);
        view.startAnimation(myAnim);
    }
    public static void setLeftToRightAnimation(Context context,View view){
        Animation animRightToLeft = AnimationUtils.loadAnimation(context, R.anim.left_to_right_animation);
        view.startAnimation(animRightToLeft);
    }
}
