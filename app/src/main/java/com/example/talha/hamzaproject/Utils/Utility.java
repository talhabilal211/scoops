package com.example.talha.hamzaproject.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.List;

/**
 * Created by Talha Bilal on 09-Aug-17.
 */

public class Utility {

    public static void LaunchActivity(Activity activity, Class to, boolean shouldFinish) {
        Intent intent = new Intent(activity, to);
        if (shouldFinish) {
            activity.startActivity(intent);
            activity.finish();
        } else {
            activity.startActivity(intent);
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static void showToast(Context context, String msg) {

        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }







}

