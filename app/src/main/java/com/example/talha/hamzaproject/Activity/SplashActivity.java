package com.example.talha.hamzaproject.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.example.talha.hamzaproject.Animations.AnimationStaticClass;
import com.example.talha.hamzaproject.R;

public class SplashActivity extends AppCompatActivity {
    private ImageView splashLogo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();

    }

    private void init() {
        splashLogo =  findViewById(R.id.splash_activity_logo_image);
        Thread thread = new Thread() {
            public void run() {
                try {
                    AnimationStaticClass.bouncerAnimation(SplashActivity.this,splashLogo);
                    sleep(3000);
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }

        };
        thread.start();
    }
}